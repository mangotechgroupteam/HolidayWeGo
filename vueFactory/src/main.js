// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import FullCalender from './components/FullCalender'
import quantityComponent from './components/quantityComponent'
import bookingDescript from './components/bookingDescript'
import InputFloatResult from './components/InputFloatResult'
import ComponentCalender from './components/ComponentCalender'
import InputCalender from './components/InputCalender'
import filterComponent from './components/filterComponent'
import componentSliderBar from './components/componentSliderBar'

global.vueFactory = global.vueFactory||{};
global.Vue = Vue;
var vueFactory = global.vueFactory;
vueFactory.InitOnGlobal=function(el='#app'){
	new Vue({el});
	return this; 
}
vueFactory.FullCalender = function(){
	Vue.component('full-calender',FullCalender);
	return this;
}
vueFactory.quantityInput = function(){
	Vue.component('quantity-input',quantityComponent);
	return this;
}
vueFactory.bookingDescript = function(){
	Vue.component('booking-description',bookingDescript);
	return this;	
}
vueFactory.InputFloatResult = function(){
	Vue.component('input-float-result',InputFloatResult);
	return this;	
}
vueFactory.ComponentCalender = function(){
	Vue.component('calendar',ComponentCalender);
	return this;	
}
vueFactory.InputCalender = function(){
	this.ComponentCalender();
	Vue.component('calendar-input',InputCalender);
	return this;	
}
vueFactory.componentSliderBar = function(){
	Vue.component('component-slider-bar',componentSliderBar);
	return this;	
}
vueFactory.filterComponent = function(){
	this.InputCalender();
	this.componentSliderBar();
	Vue.component('filter-component',filterComponent);
	return this;
}
// vueFactory.filterComponent();
// new Vue({
//   el: '#app',
//   components: { App },
//   template: '<App/>'
// })
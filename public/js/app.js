;(function(mangoFunction){
	if(!$){console.log('jQuery not installed !'); return false;}
	function componentHeader(el){
		this.init(el);
	}
	componentHeader.prototype={
		init(el){
			this.$el = $(el);
			this.$mobileBtn = this.$el.find('[mobile-btn]');
			this.$mobileBlock = this.$el.find('[mobile-block]');
			this.setBtn();
		},
		setBtn(){
			this.$mobileBtn.on('click',()=>{
				if(this.$mobileBlock.hasClass('active')){
					this.$mobileBlock.removeClass('active');
				}else{
					this.$mobileBlock.addClass('active');
					this.$mobileBlock.on('click',()=>{
						this.$mobileBlock.removeClass('active');
						this.$mobileBlock.unbind();
					});
				}
			});
		}
	}

	function mapComponent(el){
		this.regionInfo = {
			"Africa":{
				img:"/img/home/4.jpg",
				title:"Africa",
				description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
			},
            "Asia":{
            	img:"/img/home/4.jpg",
            	title:"Asia",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "Europe":{
            	img:"/img/home/4.jpg",
            	title:"Europe",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "North America":{
            	img:"/img/home/4.jpg",
            	title:"North America",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "Central America":{
            	img:"/img/home/4.jpg",
            	title:"AmCentral erica",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "South America":{
            	img:"/img/home/4.jpg",
            	title:"South America",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "Middle East":{
            	img:"/img/home/4.jpg",
            	title:"Middle East",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "Oceania":{
            	img:"/img/home/4.jpg",
            	title:"Oceania",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            },
            "Polar":{
            	img:"/img/home/4.jpg",
            	title:"Polar",
            	description:"From the snowy peaks and glaciers of New Zealand, the rainforest and red deserts of Australia, to the lush jungles of Papua New Guinea, taking in lost lagoons, pristine beaches, along the way."
            }
		}
		this.regionRef = {
			'Africa':1,
			'Asia':2,
			'Central America':3,
			'Europe':4,
			'Middle East':5,
			'North America':6,
			'Oceania':7,
			'Polar':8,
			'South America':9		
		}
		this.init(el);
	}
	mapComponent.prototype={
		init(el){
			this.$el=$(el);
			this.$btnBlock=this.$el.find('[btn-block]');
			this.$mapBlock=this.$el.find('[map-block]');
			this.mapService = mangoFunction.mapService();
			this.$worldMap = $(this.mapService.getMapString());
			this.$infoImg = this.$el.find('[info-img]');
			this.$infoTitle = this.$el.find('[info-title]');
			this.$infoDescription = this.$el.find('[info-description]');
			this.region=this.mapService.getRegions();
			this.regionBtn = {};
			this.regionArea ={};
			this.bindCountries();
			this.$mapBlock.append(this.$worldMap);
			this.goRegion('Oceania');
		},
		bindCountries(){
			for(let r in this.region){
				this.regionBtn[r] = this.generateRegionBtn(r);
				this.$btnBlock.append(this.regionBtn[r]);
				this.colloctArea(this.region[r],r);
			}
		},
		generateRegionBtn(regionName){
			let $btn=$('<li><span>'+regionName+'</span></li>');
			this.setHoverAndClick($btn,regionName);
			return $btn;
		},
		colloctArea(AreaAry,region){
			this.regionArea[region]=[];
			for(let a of AreaAry){
				let $contry = this.$worldMap.find('[region-id="'+a+'"]');
				this.setHoverAndClick($contry,region);
				this.regionArea[region].push($contry)
			}
		},
		setHoverAndClick($element,r){
			$element.hover(()=>{
				this.goRegion(r);
				this.renderRegionInfo(r);
			})
			$element.click(()=>{
				location.href='/TravelPackage/List?continent='+this.regionRef[r];
			})
		},
		goRegion(r){
			for(let btn in this.regionBtn){
				this.regionBtn[btn].removeClass('active');
				this.regionBtn[r].addClass('active');
			}
			this.$worldMap.find('[region-id]').removeClass('active');
			for(let $contry of this.regionArea[r]){
				$contry.addClass('active');
			}
		},
		renderRegionInfo(region){
			this.$infoImg.attr('src',this.regionInfo[region].img);
			this.$infoTitle.html(this.regionInfo[region].title);
			this.$infoDescription.html(this.regionInfo[region].description);
		}
	}

	function itineraryNavigation(el){
		this.init(el);
	}
	itineraryNavigation.prototype={
		init(el){
			this.$el = $(el);
			this.headerHeight = $('header .headerWrapper').height();
			this.top = this.$el.offset().top - this.headerHeight;
			this.$el.find('.iNavWrapper').css('top',this.headerHeight+'px');
			this.navMembers = [];
			this.collectNavMembers();
			this.setBtn();
			$(window).on('scroll',()=>{
				var scrollTop = $(window).scrollTop();
				if(scrollTop>=this.top){
					this.$el.addClass('active');
				}else{
					this.$el.removeClass('active');
				}
				this.checkNav(scrollTop);
			});

		},
		collectNavMembers(){
			this.navMembers = [];
			$('[nav-id]').each((index,item)=>{
				var $item = $(item);
				this.navMembers.push({id:$item.attr('nav-id'),offset:$item.offset().top - this.headerHeight - 50}); // nav height = 50
			});
		},
		checkNav(scrollTop){
			this.lightNav(this.calcResult(scrollTop).id);
		},
		setBtn(){
			this.$el.find('[nav-link]').each((index,item)=>{
				var $item = $(item);
				var NavId = $item.attr('nav-link');
				$item.on('click',()=>{
					this.navMembers.find((e,i,a)=>{
						if(e.id==NavId){
							$('html, body').animate({'scrollTop':e.offset+1},500);
						} 
					})
				})
			})
		},
		lightNav(navId){
			this.$el.find('[nav-link]').removeClass('active');
			this.$el.find('[nav-link="'+navId+'"]').addClass('active');
		},
		calcResult(scrollTop){
			var i = 0;
			for(let m of this.navMembers){
				if(m.offset>scrollTop){
					return this.navMembers[(i-1>=0)?(i-1):0];
				}
				i++
			}
			return this.navMembers[this.navMembers.length-1];
		}
	}

	function componentshellGroup(el){
		this.init(el);
	}
	componentshellGroup.prototype={
		init(el){
			this.$el = $(el);
			this.shellList = [];
			this.$el.find(' > ul').each((index,item)=>{
				var shell = new componentshell(item,()=>{this.openCallback(index)});
				this.shellList.push(shell);
				if(index==0){shell.open()}
			})
		},
		openCallback(openedIndex){}
	}

	function componentshell(el,callback){
		this.init(el,callback);
	}
	componentshell.prototype={
		init(el,callback){
			this.callback = callback;
			this.$el = $(el);
			this.$shellBlock = this.$el.find('[shell-block]');
			this.$shellBtn = this.$el.find('[shell-btn]');
			this.height = this.$shellBlock.height();
			this.setBtn();
			this.close();
		},
		setBtn(){
			this.$shellBtn.on('click',()=>{
				this.toggle();
			})
		},
		toggle(){
			if(this.$el.hasClass('active')){
				this.close();
			}else{
				this.open();
			}
		},
		close(){
			if(mangoFunction.instance.iNav){
				setTimeout(()=>{
					mangoFunction.instance.iNav.collectNavMembers();
				},1000);
			}
			this.$el.removeClass('active');
			this.$shellBlock.height(0);
		},
		open(){
			if(mangoFunction.instance.iNav){
				setTimeout(()=>{
					mangoFunction.instance.iNav.collectNavMembers();
				},1000);
			}
			this.$el.addClass('active');
			this.$shellBlock.height(this.height);
			if(this.callback){this.callback()}
		}
	}

	function componentExtandUl(el){
		this.init(el);
	}
	componentExtandUl.prototype={
		init(el){
			this.$el = $(el);
			this.$shell = this.$el.find('[shell]');
			this.height = this.$shell.height();
			this.title = this.$el.find('h4').text(); 
			this.$btn = this.renderBtn();
			this.$shell.after(this.$btn);
		},
		renderBtn(){
			if(this.height>43){
				var $template = $('<span>Read all '+this.title+'</span>');
				this.$shell.height(43);
				$template.on('click',()=>{
					if(mangoFunction.instance.iNav){
						setTimeout(()=>{
							mangoFunction.instance.iNav.collectNavMembers();
						},1000);
					}
					if(this.$el.hasClass('active')){
						this.$el.removeClass('active');
						this.$shell.height(43);
						$template.html('Read all '+this.title);
					}else{
						this.$shell.height(this.height);
						$template.html('Hide');
						this.$el.addClass('active');
					}
				});
				return $template;
			}else{
				return '';
			}
		}
	}

	function scrollLink(){
		this.init()
	}
	scrollLink.prototype={
		init(){
			$('[scroll-link-href]').on('click', function () {
		        var $base = $(this);
		        $('html, body').animate({ scrollTop: $('[scroll-link=' + $base.attr('scroll-link-href') + ']').offset().top - 139 }, 500)
		    });
		    let hash = location.hash.replace('#','');
		    if(hash!=""&&$('[scroll-link='+hash+']').length>0){
		        $('html, body').animate({ scrollTop: $('[scroll-link='+hash+']').offset().top - 139 }, 500)
		    }
		}
	}

	function popUpFrame(content){
		this.init(content);
		return this;
	}
	popUpFrame.prototype={
		init(content){
			this.$el = this.render(content);
			this.listenerList={}
			$('body').append(this.$el);
			this.setBtn();
		},
		setBtn(){
			this.$el.find('[close-btn]').click(()=>{
				this.remove();
			})
		},
		remove(){
			this.close();
			setTimeout(()=>{
				this.$el.remove();
			},200)
			this.emit('remove');
		},
		close(){
			setTimeout(()=>{
				this.$el.removeClass('active');		
			},0);
			this.emit('close');
			return this;
		},
		open(){
			setTimeout(()=>{
				this.$el.addClass('active');
			},0);
			this.emit('open');
			return this;
		},
		render(content){
			return $('<div class="popUpFrame">'+
					    '<div class="bg" close-btn></div>'+
					    '<div class="frame">'+
					        '<nav class="close" close-btn><i class="icon-cross"></i></nav>'+
					        '<div class="main">'+content+'</div>'+
					    '</div>'+
					'</div>');
		},
		on(event,callback){
			this.listenerList[event] = this.listenerList[event] || []
			this.listenerList[event].push(callback);
		},
		emit(event,arg){
			if(this.listenerList[event]){
				for(let func of this.listenerList[event]){
					func(arg)
				}
			}
		}
	}

	function popUpMessage(opt){
		this.init(opt);
	}
	popUpMessage.prototype={
		init(opt){
			let options = {
				title:null,
				content:null
			}
			for(let key in options){
				options[key] = opt[key]||options[key];
			}
			this.popUpFrame = this.create(options)
		},
		create(options){
			let h2 = '';
			let content ='';
			if(options.title){h2 = '<h2>'+options.title+'</h2>'}
			if(options.content){content = '<div>'+options.content+'</div>'}
			let template = '<div class="message">'+h2+content+'</div>';
			return mangoFunction.popUpFrame(template);
		},
		show(){
			this.popUpFrame.open();
			return this;
		},
		onClose(func){
			this.popUpFrame.on('close',func);
			return this;
		}
	}

	function vimeoFrame(el){
		this.init(el)
	}
	vimeoFrame.prototype={
		init(el){
			this.$el = $(el);
			this.popFrame = null;
			this.vimeoId = this.$el.attr('vimeo-frame');
			this.$el.on('click',()=>{
				 this.popFrame = this.create().open();
			})
		},
		create(){
			let template = '<iframe src="https://player.vimeo.com/video/'+this.vimeoId+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
			return mangoFunction.popUpFrame(template);
		}
	}

	mangoFunction.instance = {} || mangoFunction.instance;
	mangoFunction.componentHeader=function(el="header"){return new componentHeader(el);}
	mangoFunction.mapComponent=function(el="#homeMap"){return new mapComponent(el);}
	mangoFunction.itineraryNavigation=function(el="#itineraryNavigation"){
		mangoFunction.instance.iNav = new itineraryNavigation(el);
		return mangoFunction.instance.iNav;
	}
	mangoFunction.scrollLink=function(){return new scrollLink();}
	mangoFunction.componentshellGroup=function(el="#componentshellGroup"){return new componentshellGroup(el);}
	mangoFunction.componentExtandUl=function(el=".componentExtandUl"){$(el).each((index,item)=>{new componentExtandUl(item);})}
	
	mangoFunction.popUpFrame=function(content){return new popUpFrame(content);}
	mangoFunction.popUpMessage=function(opt){return new popUpMessage(opt);}
	mangoFunction.vimeoFrame=function(el="[vimeo-frame]"){$(el).each((index,item)=>{new vimeoFrame(item)})}
	window.mangoFunction=mangoFunction;
})(window.mangoFunction||{})

$(window).on('load',()=>{
	mangoFunction.componentHeader();
})

;(function(mangoFunction){
	function bookStorage(){}
	bookStorage.prototype={
		init(obj){
			localStorage.setItem('booking',JSON.stringify(obj));
		},
		get(){
			return JSON.parse(localStorage.getItem('booking'));
		},
		set(obj){
			localStorage.setItem('booking',JSON.stringify(obj));
		},
		checkBookingStatus(){
			let storage = JSON.parse(localStorage.getItem('booking'));
			if(!storage||!storage.PackageId){
		        alert('PackageId not found !')
		        history.back();
		    }
		    if(!storage.quoteOnly&&!mangoApi.getAuth()){
		        alert('Please login !')
		        location.href = '/Agent/Login/?returnurl=/TravelPackage/Itinerary/'+storage.PackageId+'?booking=true';
		    }
		}
	}

	function agentStorage(){}
	agentStorage.prototype={
		get(){
			return {
				AgentCompanyId:"2089B380-D128-E811-A850-000D3AD117E3",
				AgentStaffId:"DC0030B8-C531-E811-A851-000D3AD117E3"
			}
		}
	}

	mangoFunction.bookStorage = function(){return new bookStorage();}
	mangoFunction.agentStorage = function(){return new agentStorage();}
	window.mangoFunction=mangoFunction;
})(window.mangoFunction||{})
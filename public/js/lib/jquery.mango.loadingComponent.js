;(function(mangoFunction){
	if(!$){console.log('jQuery not installed !'); return false;}

	/*
		options = {
			insertEl: //append your loading component to this element
			text: //text shows up while loading
		}
	*/

	function loadingComponent(options){
		this.init(options);
	}
	loadingComponent.prototype={
		init(options={}){
			options.text = options.text||'Loading...';
			if(options.insertEl){
				$(options.insertEl).css('position','relative');
				this.$el = $('<div class="loadingComponent inserted"><img src="/css/img/3.gif"/></div>');
				$(options.insertEl).append(this.$el);
			}else{
				this.$el = $('<div class="loadingComponent"><img src="/css/img/3.gif"/></div>');
				$('body').append(this.$el);
			}
		},
		show(){
			setTimeout(this.$el.addClass('active'),0);
			return this;
		},
		close(){
			setTimeout(this.$el.removeClass('active'),0);	
			return this;
		}
	}
	mangoFunction.loadingComponent = function(options){return new loadingComponent(options);}
	window.mangoFunction=mangoFunction;
})(window.mangoFunction||{})